#!/bin/bash

PACKAGE="$1"

OPTS="$2"
PROLOGUE="true"
if [ "x$OPTS" == "xboost1.67" ] ; then
    PROLOGUE="echo deb https://people.debian.org/~gio/reprepro unstable main > /etc/apt/sources.list.d/gio.list"
fi

docker run -m 8g --rm giomasce/rebuilder bash -c "$PROLOGUE && eatmydata apt-get update && eatmydata apt-get build-dep -y $PACKAGE && sudo -u builder -i apt-get source $PACKAGE && sudo -u builder -i bash -c \"cd $PACKAGE-* && dpkg-buildpackage\" && echo BUILD SUCCESSFUL" 2>&1 | tee build-$PACKAGE.log
