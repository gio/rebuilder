#!/bin/bash

PACKAGE=$1

docker run -m 4g --rm giomasce/rebuilder bash -c "echo deb https://people.debian.org/~gio/reprepro unstable main > /etc/apt/sources.list.d/gio.list && eatmydata apt-get update && eatmydata apt-get build-dep -y $PACKAGE && sudo -u builder -i apt-get source $PACKAGE && sudo -u builder -i bash -c \"cd $PACKAGE-* && dpkg-buildpackage\" && echo BUILD SUCCESSFUL" | tee build-$PACKAGE.log
